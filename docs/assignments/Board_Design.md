# 5. PCB Board Design

I needed to design my own board for the course.
I designed a "Hello World" PCB board following the same steps as in <a href="http://fabacademy.org/2020/labs/oulu/students/jari-laru/week%207.%20Electronics%20design.html">here</a>.

## Board Schematics

The schematics are bellow:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/BoardDesign/schematics_ATTINY412.png" width="400" height="400" />

The components use are:
- 1 ATTiny412 <br>
- 1 1x3 RA female <br>
- 1 6 pin FTDI connector <br>
- 1 LED <br>
- 1 1k Ohm resistor <br>
- 1 10k Ohm resistor <br>
- 1 switch (button) <br>
- 1 1u capacitor <br>

I marked a mistake in the image with a red circle. I forgot to connect the VDD of the microcontroller to the VCC. Therefore, I designed a PCB board with this mistake.

## PCB

The PCB drawn PCB board is bellow:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/BoardDesign/PCB.png" width="400" height="400" />

The vias of the board are bellow:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/BoardDesign/copper_vias_PCB.jpg" width="400" height="400" />

As you can see in both figures. The VDD of the microcontroller is not connect to the VCC of the board.

## Photo of the Circuit

A photo of the circuit is bellow:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/BoardDesign/circuit_photo.jpg" width="400" height="400" />

I connected a blue jumper from the VDD of the microcontroller to the VCC to fix the mistake mentioned previously.
This is the board I used for the Fab Lab Programming course. This board has serial, LED and a Button that I can use as input and output.

## Set up Arduino IDE for the ATTiny412

I used megaTinyCore to be able to code and upload the code to my microcontroller. The instructions for the installation can be found <a href="https://github.com/SpenceKonde/megaTinyCore/blob/master/Installation.md">here</a>

The pinout that should be used when programming this microcontroller is bellow:

<img src="https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.gif?raw=true" width="600" height="600" />

The setup of the tab Tools of the Arduino IDE should be like this:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/BoardDesign/tools_setup_attiny.PNG" width="400" height="400" />

The specific mageTinyCore documentation for the microcontroller ATTiny 212/412 is <a href="https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md">here</a>. Note that in my case, I always need call "Serial.swap(1)" before calling "Serial.begin()" when using UART.

## Eagle File

<a href="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/blob/master/docs/codes/ATTiny412.zip">Zip file</a>
