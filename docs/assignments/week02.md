# 2. Interface and application programming

## Group Assigment
The group assigment is available <a href="https://dechevar19.gitlab.io/gitlab-project/assignments/week17/">here</a>. 

##Invidual Assigment

### ABSTRACT
In this work, I made serial communcation between my computer and an Arduino Uno. I connected a digital port of the Arduino Uno to the development board AVR-MT128 da Olimex to use the LED.
I wanted to control the led.

Later, I did the same application with my own board.


### Application
The social isolation limited the socialization with others. I created a simple web page where the a person could answer my questions by pressing one of the three available buttons. The buttons are "No!", "Yes", "A lot!" and when pressed it makes the LED blink one, two and three times, respectively. 
To use the application, I made a call with a person and asked to access the adress where my application is running. The person answered questions made by me with the buttons.

### Web Application
I used python as backend of my application. I created a simple web server and web page to be able to control the LED.
The version of my Python is 3.8.2.
Requirements: <br>
-Flask <br>
-pySerial

I used Flask to run a HTTP server and pySerial to make the serial communication with the Arduino IDE.
The python code used is bellow:
```
from flask import Flask
import serial
import time
from flask import render_template

server = Flask(__name__)

ser = serial.Serial('COM11', 9600)  # open serial port to the board. In this example, it is the USB port named COM11
# The function blink that makes the LED blink as many times as desired.
# The board awaits the char 'a' to turn on and char 'b' to turn off the LED
def blink(num):                 
    for i in range(num):
        ser.write(b'a')     # Turn on the LED by sending the char 'a' to the board via serial
        time.sleep(0.2)     
        ser.write(b'b')     # Turn off the LED by sending the chat 'b' to the board via serial
        time.sleep(0.2)      

# The method POST is a HTTP request. In my API, there are three possible commands that I can use.
# /Yes : Calls the function blink twice
# /no : Calls the function blink once
# /A_lot: Calls the function blink three times
@server.route('/Yes', methods=['POST'])
def yes():
    blink(2)
    return ('', 204) # 204 code is used because no action is required from the browser
@server.route('/No', methods=['POST'])
def no():
    blink(1)
    return ('', 204) # 204 code is used because no action is required from the browser
@server.route('/A_lot', methods=['POST'])
def a_lot():
    blink(3)
    return ('', 204) # 204 code is used because no action is required from the browser
# A HTTP get request to the root "/", renders an HTML page called index.html.
# This html page has buttons that makes the browser of the client communicate with the API above. The page has three buttons, one for each command above.
@server.route('/', methods=['GET']) 
def index():
    return render_template("index.html") # Sends the web page to the user

def main():
    time.sleep(2) # Wait the establishment of the serial connection           
    # host 0.0.0.0 as I want my service to be acessible from external connections
    server.run(host='0.0.0.0', port=80, debug=False,) 
    ser.close()             # close port

if __name__ == '__main__':
    main()

```
COM11 was the serial port used for my computer.
The function render_template renders an specified HTML page available in the folder templates as described <a href="https://flask.palletsprojects.com/en/1.1.x/quickstart/">here</a>. In my project, index.html is a simple html page used for the application.
The index.html is below:
```
<!DOCTYPE html>
<html lang="en">
        <head>
                  <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                      <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <title>LED answerer!</title>
        </head>

        <body>
                  <h1 style="color: black", align="center">Use the buttons to answer the questions</h1>
                  <form action="/No" method="post" align="center">
                         <input type="submit" id="No" value="No!" style="height:75px; width:100px">
                  </form>
                  <p></p>
                  <form action="/Yes" method="post" align="center">
                         <input type="submit" id="Yes" value="Yes!" style="height:75px; width:100px">
                  </form>
                  <p></p>
                  <form action="/A_lot" method="post" align="center">
                         <input type="submit" id="A_lot" value="A lot!!" style="height:75px; width:100px">
                  </form>
        </body>

</html><!DOCTYPE html>
```

In the HTML file, I use form element. The form element take inputs from the user to send it to the server. The type of of input used is the submit type where the user press a button and the form will send the action associated to the respective button to the web server. The form send the action (event) to the server based on my API where there are three resources available (/No, /Yes, /A_lot). For each different action, the server reacts in a different way as it can be seen on the python code of the server. The web server handle the event received and send commands to the Arduino Uno through the serial connection to blink the LED. 

The web page generated by the index.html is below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week02/UI_webpage.PNG" width="800" height="400" />

### Develping Using my Board
My board has a button, a LED, a ATTiny412 (microcontroller), a serial interface for communication, and other eletronic components. In this work, I programmed my board to receive information from my computer via serial communication to control the LED. The command received by serial can be to turn on or turn off the LED connected to one of my digital ports of the microcontroller. 

Information about the board and the Arduino IDE setup can be found <a href="https://henriquehilleshein.gitlab.io/olimex_pushbutton_led/assignments/Board_Design/">here</a>.  

### Code

The code used for my board is below.
```
// include the library code:
const int ledPin =  1; 

void setup() {
  // digital mode as OUTPUT as we will transmit a signal.
  pinMode(ledPin, OUTPUT);
  // Default value of the signal is LOW 
  digitalWrite(ledPin, LOW);
  // Initializes the Serial connection.
  Serial.swap(1); // For ATTiny412
  Serial.begin(9600);
}
  
void loop() {
  // check for incoming serial data:
  if (Serial.available() > 0) {
    // read incoming serial data:
    char inChar = Serial.read();
    //If 'a' is received from the serial connection, change digital pin to HIGH voltage
    if (inChar == 'a'){ 
        digitalWrite(ledPin, HIGH);
    //If 'b' is received from the serial connection, change digital pin to LOW voltage        
    } else if (inChar == 'b'){
        digitalWrite(ledPin, LOW); 
    }
  }
}
```

I am using the Arduino hardware package megaTinyCore, The pin out documentation is bellow.

<img src="https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.gif?raw=true" width="600" height="600" />

The LED is connected to the pin PA7 that is why I used the digital port number 1 in my code.

### Deployment
I tested the application locally and with a friend in Russia. I asked her some questions in a call and she answered pressing the buttons. I ran the server using the operational system Windows with the command "py led_control.py" where py is the command to run python on Windows and led_control.py is the name of the file of my script.
The video below show it working locally (127.0.0.1):

<video width="640" height="1138" controls>
  <source src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week02/Web_Interface_Controlling_Board.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>


### Challenges and Problems
I did not have problems progamming the Arduino IDE or creating the python script. I just had some difficulties to find out the serial port name (COM11 for example) to initialize the serial communication with my python program. Also, I had difficulties to make my web service available publicly in the internet to my friend be able to access the web page from Russia. All the problems that I had was because I am not very used to program and develop things on the operational system Windows.
To check the serial port name on Windows, this steps should be followed:<br>
1-Open Device Manager.<br>
2-Click on View in the menu bar and select Show hidden devices. <br>
3-Locate Ports (COM & LPT) in the list. <br>
4-Check for the com ports by expanding the same. 

To make the service available in the internet it has different configurations depending on the network and computer in hands. The person trying to run the service with availability in the internet needs to have knowledge of computer networks. In my case, my computer had a public IP. I just needed to set up my firewall to allow external connections and to set my wireless connection as private (Windows does not trust public wireless connection).


### Source Code for Download
<a href="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/blob/master/docs/codes/week_2.ino">LED Control by Serial (Arduino IDE)</a> 
<br>
<a href="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/blob/master/docs/codes/web_interface.zip">Web Interface</a> 
