# 3. Applications and implications

## What will it do?
In Finland, it is common to have public garbage dump that are big holes in the ground. They are divided in different types of waste.
Periodically a garbage truck needs to come to where is the garbage dump and take the wast out.
The idea is to use sensors to check the volume of trash filling the hole. If this information, it is possible to organize the schedules of the trucks in a effecient way. 

## Who's done what beforehand?
Some companies already has a product that does that. For example: <br>
<a href="https://sensoneo.com/product/smart-sensors/">https://sensoneo.com/product/smart-sensors/</a> <br>
<a href="https://www.urbiotica.com/en/producto/u-dump-m2m-2/">https://www.urbiotica.com/en/producto/u-dump-m2m-2/</a> <br>
<a href="https://www.wastehero.io/en/products/sensor/">https://www.wastehero.io/en/products/sensor/</a>


All of these products use ultrasound to measure the volume. The wireless communication used for the transmission of the data is LoRaWAN/Sigfox/NB-IoT for the Sensoneo's product, GPRS/3G networks for the Urbiotica's product, and LTE-M/NB-IoT/GSM/LoRaWAN for the WasteHero's product. I decided to use LoraWAN communication as Finland has availability of LoraWAN networks and because my product will transmit very little of data.

Usually, ultrasonic sensors are used to measure volume as you can see papers discussing about it: <br>
<a href="https://www.mdpi.com/1424-8220/18/5/1465/htm">https://www.mdpi.com/1424-8220/18/5/1465/htm</a> <br>
<a href="http://www.jetir.org/view?paper=JETIRW006048">http://www.jetir.org/view?paper=JETIRW006048</a>


## What will you design?
A system where it is sent to the waste management the information of the filling of all garbage dump. With this information, it is possible to know when the garbage should be taken away and also it is possible to predict the best moment to take the garbage out by truck. To be able to do that, devices with sensors are put inside of the garbage dump. The filling of the garbage dump is measured by an ultrasound sensor that will be will automatically calibrate itself when turned on. The device will have one LED to show the status of the device and switch to turn on and turn off. The trasmission of data of the device is carried out using LoRaWAN communication. To make such device, I will need to design the PCB board, the case of the board, and the software running in the device. It involves a lot of process that are described in the "What processes will be used?" section. The PCB board will stay inside of a box/case on top of the internal side of garbage dump.
With the data reaching a server, it is possible to for residents check the closest garbage dump with space for a type of waste by using an APP.


## What materials and components will be used?
These itens will be needed: <br>
-1 Battery 5V MarkerFocus (FabLab) <br>
-1 <a href="https://www.adafruit.com/product/3942">Ultrasonic sensor HC-SR04 with two 10k ohm resistors</a> <br> 
-1 <a href="https://www.microchip.com/wwwproducts/en/RN2483">RN2483 LoRaWAN microchip</a> <br>
-1 1x3 RA Female connector S5595-ND for UDPI (FabLab) <br>
-1 RGB LED CLV1A-FKB-CK1VW1DE1BB7C3C3CT-ND (FabLab) <br>
-1 1k ohm resistor for the LED (FabLab) <br>
-1 CKN10721CT-ND SWITCH SLIDE SPDT 300MA 6V (FabLab) <br>
-1 10k ohm resistor for the switch (FabLab) <br>
-1 1u F capacitor 399-4674-1-ND (FabLab) <br>
-1 Single side circuit board stock (FabLab) <br>
-1 <a href="https://www.partco.fi/en/connectors/rf-connectors/sma-connectors/sma-pcb-connectors/19071-13363534blk.html">SMA(F) S/T bulkhead jack PCB edge SMD mount</a> <br>
-1 <a href="https://www.taoglas.com/product/ti-85-2113-2dbi-868mhz-terminal-mount-dipole-antenna/">2dBi 868MHz Terminal Mount Dipole Antenna</a> <br>
-1 White PRO Series Thermoplastic Polyurethane (TPU) - 1.75mm (FabLab)

Observation: The  LoRaWAN microship has a microcontoller already. <br>
The only things not available on FabLab are the sensor, the microchip, and the antenna and its respective connector.

## Where will come from?
-Battery: No specific country <br>
-HC-SR04: No specific country. Supplier: <a href="https://www.adafruit.com/product/3942">Adafruit</a> <br>
-S5595-ND: No specific country <br>
-LoRaWAN microship: USA. Supplier: <a href="https://www.microchip.com/wwwproducts/en/RN2483">Microship</a><br>
-RGB LED: China <br>
-Switch: No specific country <br>
-Resistors: China <br>
-Capacitor: Mexico <br>
-Single side circuit board stock: USA <br>
-SMA connector: UK. Supplier: <a href="https://www.partco.fi/en/connectors/rf-connectors/sma-connectors/sma-pcb-connectors/19071-13363534blk.html">Partco</a>  <br>
-Antenna: Ireland. Supplier: <a href="https://www.taoglas.com/product/ti-85-2113-2dbi-868mhz-terminal-mount-dipole-antenna/">Taoglas</a>
-TPU: USA  

## How much will they cost?
Costs:

-Battery: €2,25 <br>
-HC-SR04: €3,95 <br>
-S5595-ND: €0,65 <br>
-LoRaWAN microchip = €11,38 <br>
-RGB LED: €0,53 <br>
-Switch: €0,57 <br> 
-Resistors = €0,01 (each) <br>
-Capacitor = €0,12 <br>
-Single side circuit board stock = €1,4 <br>
-SMA connector = €2,11 <br>
-Antenna = €6,90 <br>
-TPU = €3,00 (estimated amount of plastic used)

Total= €32,98


## What parts and systems will be made?
The PCB board, the case/box of the product and the software.
-The PCB board I will design using the EAGLE software where I will be able to connect all the eletronics used in my project.
-The case/box is used to protect the board from the external environment and to help to fix the device on a surface.
-The software has all the logic to measure the volume using the sensor and also to transmitte the data via the wireless interface.


## What processes will be used?
After checking every item needed for the whole board, I need to buy every single item. 
The schematics of the board should be made and after the PCB board project.
The eletronics should be soldered to the PCB board and then tested. After that, a code (software) should be elaborated to upload to the microcontoller to run the application desired.

Some components can be tested individually to learn how they work. Like the ultrasound sensor and the LoRaWAN microchip.

Shortly, these are the processes: <br>
- 2D design, 3D printing and laser cutting for the fabrication of the box. I still need need to consider which CAD I am going to use as I do not have experience with it <br>
- The PCB project using 2D CAD EAGLE, miling and soldering of the components. <br>
- Vacuum forming to fit the box/case with the PCB board.
- Embedded system software develpment. 

The box would be made of White PRO Series Thermoplastic Polyurethane (TPU) .

The figure below shows the whole system: <br>

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week03/diagram_garbage_dump.png" width="600" height="400" />

The data transmitted is the filling information of the dump. The server organize the trucks' schedule based on that, and also send the information of how full is the dump to mobile apps. 

## What questions need to be answered?
Is there a market for this product?
Can it be used for other applications?
How efficient will be the trucks with the information provided by the system?

## How will it be evaluated?
Firtly, I would check in laboratory how accurate is the sensor. I would put the device inside a big box and calibrate it to the size of the box. After that, I would fill the box with some kind of material with the help of a measure tape. By knowing the size of the box and how much of the box is filled, I can check if the sensor is doing correct measuraments. If it is correct, it means that I can measure sucessufully the volume. 

Secondly, I need to check how to trasmit data via the LoRaWAN modem and verify if the data received is correct.

With the sensor and the wireless interface properly tested, I can test them together. If I make the measuarement and receive the correct data in the server, it means that the whole system works.

After the in laboratory tests are finished, I would deploy the device and check the accuracy of the system as I did in laboratory and how much it increases the efficiency of the trucks that take the waste away. My requiment for the accuracy is an error of measurament of less than 5%.
