# 4. Networking and Communications

## Individual Assignment
### Introduction
In this week, I should use a protocol for communication of boards. I decided to use serial communication where the board designed by me is in the same serial bus than an Arduino UNO.
An Arduino Uno and a board designed by me have one LED each. I control which LED is turned on and turned off with serial communication. As the boards are in the same serial bus I gave address to the boards to them now who I sending the command to.
The protocol has 2 bytes. The first bity is the address and the second one is the action (turn on/off the LED).

### Arduino UNO Pinout

The pinout of the Arduino is in the figure below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week02/arduino_uno_pinout.jpg" width="400" height="400" />

I used the USB connection of the Arduino UNO to do serial communication. The pins 0 and 1 are the serial pins of the Arduino Uno and they are in the same bus than the USB. Thus, I connected the board I designed in these pins.
I used the pin 12 for the LED of the Arduino UNO.

### My Board
I am using the Arduino hardware package megaTinyCore, The pin out documentation is bellow.

<img src="https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.gif?raw=true" width="600" height="600" />

The LED is connected to the pin PA7 that is why I used the digital port number 1 in my code.
More information about the board and the Arduino IDE setup can be found <a href="https://henriquehilleshein.gitlab.io/olimex_pushbutton_led/assignments/Board_Design/">here</a>.


### Wire Connection

The wire connection between the boards and the LEDs are shown in the figure below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week04/wireconnection_week4.jpg" width="400" height="400" />

I cross connected the RX and TX of my board and Arduino board. My board is powered up by the Arduino UNO. The ground was also connect between the boards.

### Code
The code of the Arduino and my board are very similar. However, my board needs an extra line because of the Serial connection. As the <a href="https://github.com/SpenceKonde/megaTinyCore/blob/master/megaavr/extras/ATtiny_x12.md">documentation</a> says, I need to add the line Serial.swap(1) prior to Serial.begin().
Apart from that, they just have different address and different pin number for the LED.

Below is the code used for my board (ATTiny412):

```
// include the library code:
const int digitalPin =  1;
char address = 'W';
bool takeAction = false; // In case received its address, next byte is action 

void setup() {
  // digital mode as OUTPUT as we will transmit a signal.
  pinMode(digitalPin, OUTPUT);
  // Default value of the signal is LOW 
  digitalWrite(digitalPin, LOW);
  // Initializes the Serial connection.
  Serial.swap(1); // For ATTiny412
  Serial.begin(9600);
}
  
void loop() {
  // check for incoming serial data:
  if (Serial.available() > 0) {
    // read incoming serial data:
    char inChar = Serial.read();
    if (takeAction == true){
      //If 'a' is received from the serial connection, change digital pin to HIGH voltage
      takeAction = false;
      if (inChar == 'a'){ 
          digitalWrite(digitalPin, HIGH);
      //If 'b' is received from the serial connection, change digital pin to LOW voltage        
      } else if (inChar == 'b'){
          digitalWrite(digitalPin, LOW); 
      }
    }
    if (inChar == address){
      takeAction = true;  
    }
    
  }
}
```
For the Arduino UNO the line Serial.swap(1) **must** be commented or deleted. I used adress "W" for the my board and "E" for the Arduino Uno.
To turn on the LED, I need to send the character "a" and to turn off the LED, I should send the character "b".
The protocol uses 2 bytes where the first one is the address and the second one is the action. So, to turn on the LED of my board, I should send "Wa" via the serial for example. To turn off, it would be "Wb".

Observation: It is not possible to upload the code for the Arduino UNO while the serial pins between the boards are connected.



### Deployment
The deploymend as done by using the serial monitor of the Arduino IDE. The video is below:

<iframe src="https://player.vimeo.com/video/432069863" width="640" height="1138" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

### Source Code For Download

<a href="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/blob/master/docs/codes/week_4.ino">LED Control by Serial (Arduino IDE)</a>



## Group Assignment

We were supposed to send a message between two projects. We decided to have a system where my boards are controlled by a sensor that it is availabe somewhere via the internet.
I am using the Olimex as an alarm controlled by an alarm controller (Arduino Uno). The alarm controller is triggered by a sensor which is somewhere connected to the internet. 
The sensor is connected to another Arduino Uno

Here it is a representation of how the connections are done:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week04/diagram_group_ass_4.png" width="1000" height="50" />

The serial connections are USB.

The other participants of this group assignment are <a href="https://dechevar19.gitlab.io/gitlab-project/about/">Dian Echevarria Perez</a>,
<a href="https://nelsongi.gitlab.io/nelsonproject/about/">Nelson Mayedo Rodriguez</a> and
<a href="https://osmel-dev.gitlab.io/fablab-project/about/">Osmel Martínez Rosabal</a>

### SPI communication

We decided to use SPI communication between the alarm controller (Arduino) and the alarm (Olimex). We control the LED available on the Olimex. The Arduino receive commands for the LED via USB and relays it to the Olimex.

### SPI Protocol

The explanation of this protocol and code that we used as referecence for this work is available <a href="http://www.gammon.com.au/spi">here</a> 

### Set up of the Arduino IDE to program Olimex
All the steps needed to set up the Arduino IDE for Olimex can be found <a href="https://github.com/fablaboulu/olimex_arduino/blob/master/README.md">here</a>.

### Set up of the Arduino IDE to program Arduino Uno
In the Arduino IDE, select Tools->Board->Arduino Uno and Tools->Board->Programmer->ArduinoISP.

### Boards on Server Side
#### Arduino UNO pinout
The pinout of the Arduino is in the figure below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week02/arduino_uno_pinout.jpg" width="400" height="400" />

I used the USB to control the Arduino by serial communication. The SS, MOSI, MISO, SCK are the pins 10, 11, 12 and 13, respectively.

#### Olimex Schematics
The full schematics of the development board can be found <a href="https://www.olimex.com/Products/AVR/Development/AVR-MT128/resources/AVR-MT128-SCH-REV-A.pdf">here</a>

As you can see it is used a relay to control de LED. The relay is connected to the pin PA6 of the microcontroller.

In the figure below, you can see where are the pins SS, MISO and MOSI.

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week04/extension_2.PNG" width="200" height="200" />

The SS, MOSI and MISO pins of the microcontroller are the pins 6, 7 and 8 of the EXT2, respectively.

In the figure below, you can see where is the pin SCK.

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week04/icsp_sck.PNG" width="200" height="200" />

The SCK pin of the microcontroller is the pin 7 of the ICSP.

We are using the Arduino hardware package MegaCore. So we needed to check in the documentation what is the pin number used to represent a respective pin of the microcontroller in the Arduino IDE.
It can be seen in:

<img src="https://camo.githubusercontent.com/2e9fe11875f8ca873e6d9ed5c9602acdaf5c089f/68747470733a2f2f692e696d6775722e636f6d2f737765524a73332e6a7067" width="400" height="400" />

Therefore, the pin number we should use for relay (LED) is the 38. The pins used for the SPI are already set correctly by the library spi.h provided by MegaCore.

#### Wire Connection

The wires were connect as is shown in the figure below:

<img src="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/raw/master/docs/images/week04/week_4_wires.jpg" width="400" height="400" />

The SS, MOSI, MISO, SCK, ground are the red, yellow, green, blue and brown wires, respectively.


#### Boards code

The code used for the Arduino Uno (SPI Master) is below:

```
// include the library code:
#include <SPI.h>

void setup() {
  // Initializes the Serial connection.
  Serial.begin(9600);
  digitalWrite(SS, HIGH);  // ensure SS stays high for now
  SPI.begin ();

  // Slow down the master a bit
  SPI.setClockDivider(SPI_CLOCK_DIV8);
}
int count = 0;  
void loop() {
  // check for incoming serial data:
  if (Serial.available())
  {
    char c = Serial.read();
    digitalWrite(SS, LOW);    // SS is pin 10
    SPI.transfer (c);
     // disable Slave Select
    digitalWrite(SS, HIGH);
  }
}
```

The code of the Olimex (SPI slave) is below:

```
#include <SPI.h>

char buf;
volatile byte pos;
volatile boolean process_it;
const int ledPin =  38;      // the number of the LED pin

void setup() {
// initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
// turn on SPI in slave mode
  SPCR |= bit (SPE);
// have to send on master in, *slave out*
  pinMode(MISO, OUTPUT);
  
  // get ready for an interrupt 
  pos = 0;   // buffer empty
  process_it = false;

  // now turn on interrupts
  SPI.attachInterrupt();
}
// SPI interrupt routine
ISR (SPI_STC_vect)
{
  byte c = SPDR;  // grab byte from SPI Data Register
  buf = c;   
  process_it = true;
      
}  // end of interrupt routine SPI_STC_vect

void loop() {
  if (process_it){
   if(buf == 'a') {
        // turn LED on:
        digitalWrite(ledPin, HIGH);
    } else if(buf == 'b') {
       // turn LED off:
        digitalWrite(ledPin, LOW);
    }
    process_it = false;
  }  // end of flag set

}
```

### Board on the Client Side
#### Board code

The code of the sensor is below:
```
const int sensor =  2;      // the number of the LED pin
int sensorState = 0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(sensor, INPUT); 
  digitalWrite(sensor, HIGH); // Pull up resistor
}

void loop() {
  sensorState = digitalRead(sensor);
  if(sensorState == HIGH){
      Serial.print('a');
  } else {
      Serial.print('b');
  }
  // put your main code here, to run repeatedly:
  delay(20);
}
```
It is a simple code where it is checked whether the circuit is closed. We used the pin number 2 of the Arduino Uno for this purpose. To close the circuit, a wire from the pin 2 need to be connected to the ground.
When the circuit is closed, the Arduido send by the serial the character 'b' which is the normal state of the system. When the circuit is open, the Arduino sends by the Serial the character 'a' which means that the alarm should be triggered.


### Coommunication Between Boards via Internet
#### Python application
We used python as backend of our application. We created a simple HTTP server and HTTP client to be able to control the alarm.
The version of my Python is 3.8.2. <br>
Requirements: <br>
-Flask <br>
-pySerial

#### HTTP Client
The HTTP client reads the serial and sends what was received to the HTTP server through a HTTP request (POST).
The HTTP client code is below:
```
import serial
import requests
url = 'http://85.23.49.200/'

ser = serial.Serial('COM8', 9600)  # open serial port (Arduino Uno)

while True:
    command = ser.read().decode() # Read command serial
    requests.post(url+command) # Send command using REST API
ser.close()
```

#### HTTP Server
The HTTP server receives the information from the HTTP client and send the information to the alarm controller via serial.
The code of the HTTP server is below:
```
from flask import Flask
import serial
import time
from flask import render_template

server = Flask(__name__)

ser = serial.Serial('COM8', 9600)  # open serial port (Arduino Uno)

# The method POST is a HTTP request done a button is pressed.
@server.route('/a', methods=['POST'])
def led_on():
    ser.write(b'a')     # Turn on the LED
    return ('', 204) # 204 code is used because no action is required from the browser
@server.route('/b', methods=['POST'])
def led_off():
    ser.write(b'b')     # Turn off the LED
    return ('', 204) # 204 code is used because no action is required from the browser
def main():
    time.sleep(2) # Wait the establishment of the serial connection
    # host 0.0.0.0 as I want my service to be acessible from external connections
    server.run(host='0.0.0.0', port=80, debug=False,)
    ser.close()             # close port

if __name__ == '__main__':
    main()
```

### Deployment
We recorded a video where the sensor and the alarm were in different houses. So, we used the internet to make the communication.
The video is below:

<iframe src="https://player.vimeo.com/video/419538095" width="640" height="564" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

### Source Code for Download

**Server Side**
<br>
<a href="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/blob/master/docs/codes/week4_2.ino">Arduino Uno</a> 
<br>
<a href="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/blob/master/docs/codes/week4_olimex_2.ino">Olimex</a>
<br>
<a href="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/blob/master/docs/codes/led_control_api.py">Python</a>

**Client Side**
<br>
<a href="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/blob/master/docs/codes/week4_sensor.ino">Arduino Uno</a>
<br>
<a href="https://gitlab.com/HenriqueHilleshein/olimex_pushbutton_led/-/blob/master/docs/codes/sensor_api.py">Python</a>

