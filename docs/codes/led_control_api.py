from flask import Flask
import serial
import time
from flask import render_template

server = Flask(__name__)

ser = serial.Serial('COM8', 9600)  # open serial port (Arduino Uno)

# The method POST is a HTTP request done a button is pressed.
@server.route('/a', methods=['POST'])
def led_on():
    ser.write(b'a')     # Turn on the LED
    return ('', 204) # 204 code is used because no action is required from the browser
@server.route('/b', methods=['POST'])
def led_off():
    ser.write(b'b')     # Turn off the LED
    return ('', 204) # 204 code is used because no action is required from the browser
def main():
    time.sleep(2) # Wait the establishment of the serial connection           
    # host 0.0.0.0 as I want my service to be acessible from external connections
    server.run(host='0.0.0.0', port=80, debug=False,) 
    ser.close()             # close port

if __name__ == '__main__':
    main()
