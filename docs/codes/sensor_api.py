import serial
import requests
url = 'http://85.23.49.200/'


ser = serial.Serial('COM8', 9600)  # open serial port (Arduino Uno)

while True:
    command = ser.read().decode() # Read command serial
    requests.post(url+command) # Send command using REST API
ser.close()
