// include the library code:
#include <SPI.h>

void setup() {
  // Initializes the Serial connection.
  Serial.begin(9600);
  digitalWrite(SS, HIGH);  // ensure SS stays high for now
  SPI.begin ();

  // Slow down the master a bit
  SPI.setClockDivider(SPI_CLOCK_DIV8);
}
int count = 0;  
void loop() {
  // check for incoming serial data:
  if (Serial.available())
  {
    char c = Serial.read();
    digitalWrite(SS, LOW);    // SS is pin 10
    SPI.transfer (c);
     // disable Slave Select
    digitalWrite(SS, HIGH);
  }
}
