#include <SPI.h>

char buf;
volatile byte pos;
volatile boolean process_it;
const int ledPin =  38;      // the number of the LED pin

void setup() {
// initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
// turn on SPI in slave mode
  SPCR |= bit (SPE);
// have to send on master in, *slave out*
  pinMode(MISO, OUTPUT);
  
  // get ready for an interrupt 
  pos = 0;   // buffer empty
  process_it = false;

  // now turn on interrupts
  SPI.attachInterrupt();
}
// SPI interrupt routine
ISR (SPI_STC_vect)
{
  byte c = SPDR;  // grab byte from SPI Data Register
  buf = c;   
  process_it = true;
      
}  // end of interrupt routine SPI_STC_vect

void loop() {
  if (process_it){
   if(buf == 'a') {
        // turn LED on:
        digitalWrite(ledPin, HIGH);
    } else if(buf == 'b') {
       // turn LED off:
        digitalWrite(ledPin, LOW);
    }
    process_it = false;
  }  // end of flag set
    

}
