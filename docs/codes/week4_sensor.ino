const int sensor =  2;      // the number of the LED pin
int sensorState = 0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(sensor, INPUT);
  digitalWrite(sensor, HIGH);
}

void loop() {
  sensorState = digitalRead(sensor);
  if(sensorState == HIGH){
      Serial.print('a');
  } else {
      Serial.print('b');
  }
  // put your main code here, to run repeatedly:
  delay(20);
}
