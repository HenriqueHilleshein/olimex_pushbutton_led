// include the library code:
const int digitalPin =  12; 

void setup() {
  // digital mode as OUTPUT as we will transmit a signal.
  pinMode(digitalPin, OUTPUT);
  // Default value of the signal is LOW 
  digitalWrite(digitalPin, LOW);
  // Initializes the Serial connection.
  //Serial.swap(1); // For ATTiny412
  Serial.begin(9600);
}
  
void loop() {
  // check for incoming serial data:
  if (Serial.available() > 0) {
    // read incoming serial data:
    char inChar = Serial.read();
    //If 'a' is received from the serial connection, change digital pin to HIGH voltage
    if (inChar == 'a'){ 
        digitalWrite(digitalPin, HIGH);
    //If 'b' is received from the serial connection, change digital pin to LOW voltage        
    } else if (inChar == 'b'){
        digitalWrite(digitalPin, LOW); 
    }
  }
}
